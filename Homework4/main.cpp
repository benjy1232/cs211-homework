#include<iostream> //Including library iostream
#include<fstream> //Including library fstream
using namespace std; //Declaration of namespace std

int str_length(char str[]) { //Declaration of function str_length that returns type int that takes in a character array
    int i = 0; //Declaration of variable i of type int with initialization 0
    while (str[i] != '\0') { //While str[i] != 0 do this
        i++; //Increment i
    }
    return i; //When it exits the loop return i
}

bool str_compare(char str[], char str_compare[], int length) { //Declaration of function str_compare with return type bool and inputs of two character arrays and one int
    for (int i = 0; i < length; i++) { //For loop with int i = 0; while i is less than length, increment i
        if (str[i] != str_compare[i]) { //Checks to see if str[i] is equal to str_compare[i]
            return false; //If they are not equal return false
        }
    }
    return true; //If they are equal til the very end then return true
}

void char_swap(char str[]) { //Declaration of function char_swap with return type void and input character array
    if (str_length(str) < 6) { //If the length is not equal to or greater than 6 and has not gone through str_length
        return; //Exit the function
    }
    char caesar[] = "Caesar"; //Declaration of character array 'caesar' with a value of "Caesar"
    char brutus[] = "Brutus"; //Delcaration of character array 'brutus' with a value of "Brutus"
    char new_str[7] = {}; //Creation of new string of size 7
    for(int i = 0; i < 6; i++){ //Initialization of the array as being the first 6 characters of the string
        new_str[i] = str[i]; //set new_str[i] = str[i]
    }
    new_str[6] = '\0'; //Set the final term of new_str = '\0'

    if (str_compare(str, caesar, 6)) { //If the funciton str_compare returns true with the value "Caesar", then do enter this if-statement
        for (int j = 0; j < 6; j++) { //For loop with int j = 0; while j is less than 6; increment j
            new_str[j] = brutus[j]; //set str[j] = brutus[j]
        }
    }
    else if (str_compare(str, brutus, 6)) { //Else if the function str_compare returns true with str and a value of brutus, enter this statement
        for (int j = 0; j < 6; j++) { //For loop with int j = 0; while j is less than 6; increment j
            new_str[j] = caesar[j]; //Set str[j] = caesar[j]
        }
    }
    for(int k = 0; k < 6; k++){ //Set str equal to new_str
        str[k] = new_str[k];
    }
    return; //Exit the function
}

int main(void) { //Declaration of function main
    ifstream ifs; //Declaration of variable ifs of type ifstream
    ofstream ofs; //Declaration of variable ofs of type ofstream
    char str[1000] = {}; //Declaration of character array of size 1000 called str
    char single_word[1000] = {};
    int x = 0;
    ifs.open(".\\source_file.txt"); //ifs = .\\source_text.txt
    ofs.open(".\\target_file.txt"); //ofs = .\\tart_file.txt
    while (!ifs.eof()) { //While ifs hasn't reached the end of the file
        ifs.getline(str, 1000); //Get a line from ifs and store that into string
        int prev = 0; //Set int previous equal to 0
        for (int i = 0; i <= str_length(str); i++) { //For loop that goes for however long the string is
            if (str[i] == ' ' || str[i] == '\0') { //If the current character is blank space then do the rest of this
                for (int j = prev, x = 0; j < i; j++, x++) { //For however long the difference between the beginning of this word and the end of it do this
                    single_word[x] = str[j]; //Set single_word[x] equal to str[j]
                    single_word[x + 1] = '\0'; //Set the last character of single_word equal to '\0'
                }
                char_swap(single_word); //Does all the checks to see if the word is caesar or brutus and if it is then returns the change
                for (int a = prev, x = 0; a < i; a++, x++) { //sets the current word in the string equal to whatever char_swap returned
                    str[a] = single_word[x]; //sets str[a] = single_word[x]
                    single_word[x] = '\0'; //sets all the characters in use by single_word equal to '\0'
                }
                prev = i + 1; //Start the beginning of previous word to be equal to current index (blank space) + 1
            }
        }
        ofs << str << endl; //Add str to ofs
    }
    ifs.close(); //close ifs file
    ofs.close(); //close ofs file
}